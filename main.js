// simplified A* algorithm to find path
var PathFinder = require('./astar.js');


var zombieSet1 = [
    {x:1,y:0,m:0},
    {x:-1,y:0,m:0},
    {x:10,y:10,m:1000},
    {x:10,y:-10,m:1000}
];


var zombieSet2 = [
    {x:1,y:1,m:0},
    {x:2,y:2,m:0},
    {x:3,y:3,m:0}
];

var zombieSet3 = [
    {x:10,y:10,m:1000},
    {x:-10,y:10,m:1000},
    {x:10,y:-10,m:1000},
    {x:-10,y:-10,m:1000},
    {x:20,y:20,m:2000}
];

var map = new Array(2000*2000);

for (var indexMap = 0; indexMap < 2000*2000; indexMap++) {
    map[indexMap] = 0;
}

var start = {x:0,y:0,m:0};

var case1 = find(start, zombieSet1, 0, true);
var case2 = find(start, zombieSet2, 0, true);
var case3 = find(start, zombieSet3, 0, true);

console.log(case1);
console.log(case2);
console.log(case3);


function find(start, a, time, charged) {

    // if there are no elements in array - stop here
    if(a.length === 0) {
        return 0;
    } else {
        // otherwise map each element in array and invoke 'find' function recursively and reduce to maximum value then
        return a.map(function(x) {

            // set current element as end point
            var end = x;

            // how much time takes way to end element
            var p = new PathFinder(start.x, start.y, end.x, end.y, map).time();

            // check if 'Zombie Smasher' is not charged - wait 750ms then
            if(!charged) {
                p = p < 750 ? 750 : p;
                charged = true;
            } else {
                charged = false;
            }

            // if we are able to reach and hit current zombie
            if((time+p) <= (end.m + 1000)) {

                // update current time
                time = Math.max(end.m, time+p);

                // filter array: exclude current end element and leave elemens that fits current time + 1000ms (zombie stands in place)
                var na = a.filter(function(y) {
                    return end!==y && (y.m + 1000) > time;
                });

                // add 1 and invoke 'find' for new array and current time
                return 1 + find(end, na, time, charged);
            }

            return 0;

        }).reduce(function(p,c) {
            // get maximum of values
            return Math.max(p,c);
        });
    }

}