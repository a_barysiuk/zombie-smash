function PathFinder (startX, startY, endX, endY, map) {

    this.openList = [];
    this.closeList = [];

    var startNode = new Node(startX, startY);

    this.openList.push(startNode);

    var currentNode;
    while (currentNode = this.openList.shift()) {
        this.closeList.push(currentNode);
        if(currentNode.x == endX && currentNode.y == endY) {
            var curr = currentNode;
            this.path = [];
            while(curr.parent) {
                this.path.push(curr);
                curr = curr.parent;
            }
            return this;
        }
        for (var x = currentNode.x - 1; x <= currentNode.x + 1; x++) {
            for (var y = currentNode.y - 1; y <= currentNode.y + 1; y++) {
                if (x >= -100 && y >= -100 && x < 100 && y < 100) {
                    var isAlreadyClose = this.isAlreadyList(this.closeList, x, y);
                    if (map[x*1000+y] != 1 && isAlreadyClose == -1) {
                        var isAlreadyOpen = this.isAlreadyList(this.openList, x, y);
                        if (isAlreadyOpen == -1) {
                            var neighbor = new Node(x, y);
                            neighbor.parent = currentNode;
                            neighbor.calculateH(endX, endY);
                            neighbor.g = currentNode.g + neighbor.calculateG(currentNode.x, currentNode.y);
                            neighbor.f = neighbor.g + neighbor.h;
                            this.openList.push(neighbor);
                        }
                        else {
                            var g1 = this.openList[isAlreadyOpen].g;
                            var g2 = currentNode.g + this.openList[isAlreadyOpen].calculateG(currentNode.x, currentNode.y);
                            if (g1 > g2) {
                                this.openList[isAlreadyOpen].parent = currentNode;
                                this.openList[isAlreadyOpen].g = g2;
                            }
                        }
                    }
                }
            }
        }
        if (this.openList.length > 1) {
            this.openList.sort(this.sortList);
        }
    }
    return [];
}
PathFinder.prototype = {
    openList: null,
    closeList: null
};


PathFinder.prototype.isAlreadyList = function (list, x, y) {
    for (var i in list) {
        if (list[i].x == x && list[i].y == y)
            return i;
    }
    return -1;
};


PathFinder.prototype.sortList = function(a, b) {
    if (a.f > b.f)
        return 1;
    else if (a.f < b.f)
        return -1;
    else
        return 0;
};

PathFinder.prototype.time = function() {

    return this.path[0].g;
};

function Node(x, y) {
    this.x = x;
    this.y = y;
}
Node.prototype = {
    g: 0,
    h: 0,
    f: 0,
    parent: null
};

Node.prototype.calculateH = function (endX, endY) {
    this.h = (Math.abs(this.x - endX) + Math.abs(this.y - endY)) * 10;
};

Node.prototype.calculateG = function () {
    return 100;
};

module.exports = PathFinder;